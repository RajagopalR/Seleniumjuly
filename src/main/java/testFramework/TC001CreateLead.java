package testFramework;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.cucumber.cucumberexpressions.CaptureGroupTransformer;

public class TC001CreateLead extends ProjectMethods{
	
	
	@BeforeClass /*(groups = "Common")*/
	public void setData()
	{
		startTestCase("CreateLead", "Create a new Lead", "Rajagopal", "UnitTest");
		fileName = "createlead";
	} 
	
	
	@Test (/*groups = "Smoke"*/ dataProvider = "FetchData")
	
	public void createLead(String cName, String fName, String lName, String marketID, String providerID) {
		
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		WebElement elecreatelead = locateElement("linktext", "Create Lead");
		click(elecreatelead);
		WebElement elecompname = locateElement("id", "createLeadForm_companyName");
		type(elecompname, cName);
		WebElement elefname = locateElement("id", "createLeadForm_firstName");
		type(elefname, fName);
		WebElement elelname = locateElement("id", "createLeadForm_lastName");
		type(elelname, lName);
		WebElement elemc = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(elemc, marketID);
		WebElement elestate = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingIndex(elestate, Integer.parseInt(providerID));
		WebElement eleCreateLead = locateElement("name", "submitButton");
		click(eleCreateLead);
		String fname = locateElement("id", "viewLead_firstName_sp").getText();
		if(fname.equalsIgnoreCase("Rajagopal")) {
			System.out.println("Create Lead Successfull for "+fname);
		} else if(fname.equalsIgnoreCase("Uma"))
				{
			
				System.out.println("Create Lead Successful for "+fname);
		}
	}
		
	
	

}
