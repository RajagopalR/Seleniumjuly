package testFramework;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.SysexMessage;

import org.apache.commons.io.FileUtils;
import org.omg.PortableServer.RequestProcessingPolicyOperations;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeMethods extends ExtentHtmlReport implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;


	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./driver/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The Browser "+browser+" Launched Successfully", "PASS");
		} catch (WebDriverException e) {
			reportStep("The Browser "+browser+" not Launch", "FAIL");
		} finally {
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "name"  : return driver.findElementByName(locValue);
			case "tag"   : return driver.findElementByTagName(locValue);
			case "linktext" : return driver.findElementByLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element is not found", "FAIL");

		} catch (Exception e) {
			reportStep("Unknown Exception", "FAIL");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue);
		}catch (NoSuchElementException e) {
			reportStep("The Element is not found", "FAIL");
		} catch (Exception e) {
			reportStep("Unknown Exception", "FAIL");
		}
		return null;
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("The data "+data+" is Entered Successfully", "PASS");
		} catch (WebDriverException e) {
			reportStep("The data "+data+" is not entered", "FAIL");
		} finally {
			takeSnap();
		}
	}


	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element "+ele+" Clicked Successfully", "PASS");

		} catch (Exception e) {
			reportStep("The Element "+ele+" is not click", "FAIL");
		}
	}


	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element "+ele+" Clicked Successfully", "PASS");
		} catch (WebDriverException e) {
			reportStep("The Element "+ele+" is not click", "FAIL");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		String eleText;
		try {
			eleText = ele.getText();
			reportStep("The Element text is: "+eleText, "PASS");
		} catch (NoSuchElementException e) {
			reportStep("The Element is not found", "FAIL");
		}

		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			reportStep("The DropDown Is Selected with VisibleText "+value, "PASS");
		} catch (Exception e) {
			reportStep("The DropDown Is  not Selected with VisibleText "+value, "FAIL");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			reportStep("The DropDown Is Selected with IndexValue "+index, "PASS");
		} catch (Exception e) {
			reportStep("The DropDown Is not Selected with IndexValue "+index,"FAIL");
		} finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		String title = driver.getTitle();
		if(title.equalsIgnoreCase(expectedTitle)) {
			reportStep("The Title is expected"+title, "PASS");
		}else reportStep("The title is not matched", "FAIL");
		
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> setOfWindows = driver.getWindowHandles();
			List<String> listOfWindows = new ArrayList<String>();
			listOfWindows.addAll(setOfWindows);
			driver.switchTo().window(listOfWindows.get(index));
			reportStep("Control switched to the window: "+driver.getTitle(), "PASS");
		} catch (NoSuchWindowException e) {
			reportStep("Control is not switched", "PASS");
		} finally {
			takeSnap();

		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {

		try {
			driver.switchTo().alert().accept();
			reportStep("Alert accepted", "PASS");
		} catch (NoAlertPresentException e) {
			reportStep("There is no Alert Present", "FAIL");
			
		}

	}

	@Override
	public void dismissAlert() {
		try {

			driver.switchTo().alert().dismiss();
			reportStep("Alert dismissed", "PASS");
		} catch (NoAlertPresentException e) {
			reportStep("There is no Alert Present", "FAIL");
		}

	}

	@Override
	public String getAlertText() {
		String alertText;
		try {
			alertText = driver.switchTo().alert().getText();
			reportStep("The Alert text "+alertText, "PASS");
		} catch (NoAlertPresentException e) {
			reportStep("There is no Alert Present","FAIL");
		}
		
		return null;
	}


	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./screenshots/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
			reportStep("Screenshot has been taken and saved to the file successfilly", "PASS");
		} catch (IOException e) {
			reportStep("There is no such file found", "FAIL");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
		reportStep("The current browser has been closed", "PASS");
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
		reportStep("The entire browser of the current session have been closed ", "PASS");

	}


	@Override
	public void webDriverWait(WebElement ele, int seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOf(ele));

	}


}
