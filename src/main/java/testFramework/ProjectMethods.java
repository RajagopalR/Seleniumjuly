package testFramework;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class ProjectMethods extends SeMethods {
	
	@DataProvider (name = "FetchData")
	 public Object[][] getData() throws IOException {
		 Object[][] excelData = ReadExcel.getExcelData(fileName);
		return excelData;
		 
	 }
	
@BeforeMethod /*(groups = "Common")*/
@Parameters ({"browser", "url", "username", "password" })
	public void login1(String broswerName, String url, String uName, String pswd) {
		startApp(broswerName, url);
		WebElement eleusername =locateElement("id", "username");
		type(eleusername, uName);
		WebElement elepassword = locateElement("id", "password");
		type(elepassword, pswd);
		WebElement elesubmit = locateElement("class", "decorativeSubmit");
		click(elesubmit);
		WebElement elecrmsfa = locateElement("xpath", "//div[@for='crmsfa']/a");
		click(elecrmsfa);
		System.out.println("Login Successful");
	}

@AfterMethod /*(groups = "Common")*/
public void close() {
	closeAllBrowsers();
	System.out.println("Browser has been closed");
}

}
