package testFramework;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentHtmlReport {

	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testCaseName,testCaseDescription,author,category,fileName;
	@BeforeSuite /*(groups = "Common")*/
	public void startResultt()
	{
		ExtentHtmlReporter html = new ExtentHtmlReporter("./Report/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);

	}
	
	public void startTestCase(String testCaseName, String testCaseDescription, String author, String category )
	{
		test = extent.createTest(testCaseName,testCaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void reportStep(String desc, String status)
	{
		if(status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		}if(status.equalsIgnoreCase("fail")) {
			test.fail(desc);
		}
	}
	@AfterSuite /*(groups = "Common")*/
	public void loadResults()
	{
		extent.flush();
	}

}
