package testFramework;

import org.testng.annotations.Test;

public class TC004DeleteLead extends ProjectMethods {
	
	@Test (groups = "Regression" , dependsOnGroups = "Sanity")
	public void deleteLead()
	{
		System.out.println("Lead has been deleted");
	}

}
