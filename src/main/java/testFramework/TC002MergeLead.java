package testFramework;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class TC002MergeLead extends ProjectMethods {

	@Test (enabled = false)

	public void mergeLead() throws InterruptedException {

		WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
		click(eleLeads);
		WebElement eleMergeLeads = locateElement("linktext", "Merge Leads");
		click(eleMergeLeads);
		WebElement eleFromLeadImg = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(eleFromLeadImg);
		//Handling windows here to switch to interest window.
		switchToWindow(1);
		WebElement eleFromLeaderID = locateElement("name", "id");
		type(eleFromLeaderID, "10132");
		takeSnap();
		WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleTable = locateElement("class", "x-grid3-row-table");
		webDriverWait(eleTable, 80);
		Thread.sleep(3000);
		WebElement eleFromLeadSrch = locateElement("xpath", "//a[text()='10132']");
		clickWithNoSnap(eleFromLeadSrch);
		switchToWindow(0);

		WebElement eleToLeadImg = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleToLeadImg);
		switchToWindow(1);
		WebElement eleToLeadId = locateElement("name", "id");
		type(eleToLeadId, "10133");
		WebElement eleFindLeadSrch = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeadSrch);
		// Defining Web driver wait to wait for given seconds
	//	webDriverWait(eleTable, 80);
		Thread.sleep(3000);
		WebElement eleToLeadSrch1 = locateElement("xpath", "//a[text()='10133']");
		clickWithNoSnap(eleToLeadSrch1);
		switchToWindow(0);
		WebElement eleMerge = locateElement("class", "buttonDangerous");
		clickWithNoSnap(eleMerge);
		// Get Alert Text
		getAlertText();
		//Accept Alert
		acceptAlert();
		WebElement eleFindLeadSrch1 = locateElement("linktext", "Find Leads");
		click(eleFindLeadSrch1);
		WebElement eleLeadID = locateElement("name", "id");
		type(eleLeadID, "10132");
		//search for Lead ID above
		WebElement eleLeadSrch = locateElement("class", "x-btn-text");
		click(eleLeadSrch);
		//Take Snap of Merged Lead Search Result
		takeSnap();

	}

}