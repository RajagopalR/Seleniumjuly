package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		//driver.findElementByClassName("ng-star-inserted").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allwindows = driver.getWindowHandles();
		System.out.println("Total active windows "+allwindows.size());
		List<String> listofWindows = new ArrayList<String>();
		listofWindows.addAll(allwindows);
		String secondwindow = listofWindows.get(1);
		driver.switchTo().window(secondwindow);
        String secondWindowTitle = driver.getTitle();
        System.out.println(secondWindowTitle);
        System.out.println(driver.getCurrentUrl());
        
        
        
        
        

	}

}
