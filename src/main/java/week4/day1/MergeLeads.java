package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLeads {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByXPath("//div[@for='crmsfa']/a").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindows = new ArrayList<String>();
		listOfWindows.addAll(allWindows);
		driver.switchTo().window(listOfWindows.get(1));
		System.out.println(driver.getTitle());
		driver.findElementByName("id").sendKeys("10063");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		WebDriverWait wait = new WebDriverWait(driver, 70);
 		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOf(driver.findElementByClassName("x-grid3-row-table")));
		List<WebElement> allrows = driver.findElementsByXPath("//table[@class='x-grid3-row-table']//a");
		allrows.get(0).click();
		driver.switchTo().window(listOfWindows.get(0));
		System.out.println(driver.getTitle());
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Set<String> allWindows1 = driver.getWindowHandles();
		listOfWindows.addAll(allWindows1);
		System.out.println(listOfWindows.size());
		driver.switchTo().window(listOfWindows.get(3));
		driver.findElementByName("id").sendKeys("10063");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		WebDriverWait wait1 = new WebDriverWait(driver, 70);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOf(driver.findElementByClassName("x-grid3-row-table")));
		List<WebElement> allrows1 = driver.findElementsByXPath("//table[@class='x-grid3-row-table']//a");
		allrows1.get(0).click();
		driver.switchTo().window(listOfWindows.get(0));
		driver.findElementByClassName("buttonDangerous").click();
		System.out.println("Here is the alert :" +driver.switchTo().alert().getText());
		driver.switchTo().alert().accept(); 
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys("10062");
		driver.findElementByClassName("x-btn-text").click();
		/*Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOf(driver.findElementByClassName("x-grid3-row-table")));
		List<WebElement> allrows2 = driver.findElementsByXPath("//table[@class='x-grid3-row-table']//a");
		System.out.println("Message "+driver.findElementByClassName("x-paging-info").getText());*/
		File source = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./screenshots/FineLeads1.jpg");
		FileUtils.copyFile(source, dest);
		System.out.println("Screenshot has been taken and saved under screesnshots");
		driver.close();
				
		
	}

}
