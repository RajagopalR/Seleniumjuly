package projectDay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FixMyBug {

	@Test
	public void startApp() throws InterruptedException {

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.myntra.com/");

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();


		// Find the count of Jackets
		String leftCount = 
				driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span")
				.getText();
		System.out.println(leftCount.replaceAll("\\D", ""));
		String newLeftCount = leftCount.substring(1, leftCount.length()-1);
		newLeftCount.trim();
		System.out.println(newLeftCount);

		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span")
				.getText();
		String newrightCount = rightCount.substring(2, 6);
		System.out.println(newrightCount);

		// If both count matches, say success
		if(newLeftCount.equals(newrightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("//h4[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("/D", ""));
		}
		System.out.println(onlyPrice.size());

		// Sort them 
		String max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);
		
		//driver.close(); NoSuchSessionException occured due to this

		// Print Only Allen Solly Brand Minimum Price
		//Error - Xpath was wrong
		driver.findElementByClassName("brand-more").click();
		driver.findElementByXPath("(//input[@value='Allen Solly']/..)/div").click();
		//driver.findElementByXPath("//ul[@class='FilterDirectory-indices']/following-sibling::span").click();

		// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		onlyPrice = new ArrayList<String>();
		

		for (WebElement productPrice1 : allenSollyPrices) {
			onlyPrice.add(productPrice1.getText().replaceAll("//D", ""));
		}
		System.out.println(onlyPrice.size());

		// Get the minimum Price 
		String min = Collections.min(onlyPrice);

		// Find the lowest priced Allen Solly
		System.out.println(min);
		//driver.close();


	}

}