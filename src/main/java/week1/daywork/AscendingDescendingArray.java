package week1.daywork;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class AscendingDescendingArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the length of the array");
		Scanner scan=new Scanner(System.in);
		int m=scan.nextInt();
		Integer[] a=new Integer[m];
		System.out.println("Enter "+m + " values of array");
		for(int i=0; i<m; i++) {
			a[i]=scan.nextInt();
		}
		Arrays.sort(a);
		System.out.println("The ascending order of the given array: "+Arrays.toString(a) );
		Arrays.sort(a, Collections.reverseOrder());
		System.out.println("The dscending order of the given array: "+Arrays.toString(a));
	}

}
