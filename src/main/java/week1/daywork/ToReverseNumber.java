package week1.daywork;

import java.util.Scanner;

public class ToReverseNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the number");
		int number=scan.nextInt();
		int reverse=0;
		while(number>0) {
			int n1=number%10;
			int n2=number/10;
			number=n2;
			reverse=n1+(reverse*10);
		}
		System.out.println("Reverse of the given number: "+reverse);

	}

}
