package week1.daywork;

import java.util.Scanner;

public class ToFindMonthDays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the number");
		int month=scan.nextInt();
		switch(month) 
		{
		case 1:
			System.out.println("The 1st month of the year is January");
			System.out.println("Number of days in the month of January is 31");
			break;
		case 2:
			System.out.println("The 1st month of the year is February");
			System.out.println("Number of days in the month of February is 28");
			break;
		case 3:
			System.out.println("The 1st month of the year is March");
			System.out.println("Number of days in the month of March is 31");
			break;
		case 4:
			System.out.println("The 1st month of the year is April");
			System.out.println("Number of days in the month of April is 30");
			break;
		case 5:
			System.out.println("The 1st month of the year is May");
			System.out.println("Number of days in the month of May is 31");
			break;
		case 6:
			System.out.println("The 1st month of the year is June");
			System.out.println("Number of days in the month of June is 30");
			break;
		case 7:
			System.out.println("The 1st month of the year is July");
			System.out.println("Number of days in the month of July is 31");
			break;
		case 8:
			System.out.println("The 1st month of the year is August");
			System.out.println("Number of days in the month of August is 31");
			break;
		case 9:
			System.out.println("The 1st month of the year is September");
			System.out.println("Number of days in the month of September is 30");
			break;
		case 10:
			System.out.println("The 1st month of the year is October");
			System.out.println("Number of days in the month of October is 31");
			break;
		case 11:
			System.out.println("The 1st month of the year is November");
			System.out.println("Number of days in the month of November is 30");
			break;
		case 12:
			System.out.println("The 1st month of the year is December");
			System.out.println("Number of days in the month of December is 31");
			break;

		default :
			System.out.println("There are only 12 months in a year");

		}

	}

}
