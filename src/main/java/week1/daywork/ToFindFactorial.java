package week1.daywork;

import java.util.Scanner;

public class ToFindFactorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the number to find its factorial");
		int number=scan.nextInt();
		int fact=1;
		for(int i=1; i<=number; i++) {
			fact=fact*i;
		}
		System.out.println("The Factorial of the given number is "+fact);
}

}
