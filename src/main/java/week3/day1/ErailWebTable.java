package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ErailWebTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//https://erail.in/
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		if(selected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 70);
		WebElement table =  wait.until(ExpectedConditions.visibilityOf(driver.findElementByXPath("//table[@class='DataTable TrainList']")));
		
		List<WebElement> rowlist = table.findElements(By.tagName("tr"));
		System.out.println("No of Trains: " +rowlist.size());
		
		for (WebElement eachtrain : rowlist) {
			List<WebElement> trainnames = eachtrain.findElements(By.tagName("td"));
			System.out.println(trainnames.get(1).getText());
			
			
		}

		
		driver.close();

	}

}
