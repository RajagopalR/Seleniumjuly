package week2.day1;

public interface Television {

	public void switchOn(int button);
	public void switchOn(String powerButton);
	public void changeChannel(String channelName);
}
