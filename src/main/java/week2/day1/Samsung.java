package week2.day1;

public class Samsung extends Media implements Television {

	@Override
	public void switchOn(int button) {
		// TODO Auto-generated method stub
		System.out.println("TV Switched ON");
		
	}

	@Override
	public void switchOn(String powerButton) {
		// TODO Auto-generated method stub
		System.out.println("TV Switched ON");
		
	}

	@Override
	public void changeChannel(String channelName) {
		// TODO Auto-generated method stub
		System.out.println("Channel has been changed to "+channelName);
	}

}
