package week2.day1;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapProgram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<String, Integer> alltvs= new LinkedHashMap<String, Integer>();
		alltvs.put("Onida", 2);
		alltvs.put("Samsung", 1);
		alltvs.put("LG", 3);
		alltvs.put("Acer", 4);
		alltvs.put("Sony", 2);
		alltvs.put("Onida", 1);
		int sum=0;
		for (Entry<String, Integer> tvs: alltvs.entrySet()) {
			int count=tvs.getValue();
			sum=sum+count;			
		}
		System.out.println("Total Number of TVs " +sum);
		Set<String> lasttv=alltvs.keySet();
		System.out.println(lasttv);
		List<String> lasttvset = new ArrayList<String>();
		lasttvset.addAll(lasttv);
		System.out.println("Last TV Count: " +alltvs.get(lasttvset.get(lasttvset.size()-1)));
	}

}
