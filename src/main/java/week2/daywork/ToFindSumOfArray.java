package week2.daywork;

import java.util.Scanner;

public class ToFindSumOfArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter the length of Array");
		int a=scan.nextInt();
		Integer[] b=new Integer[a];
		int sum=0;
		System.out.println("Enter the Array values");		
		for(int i=0; i<b.length; i++) {
			b[i]=scan.nextInt();
			sum=sum+b[i];
		}
		System.out.println("Sum of the array: "+sum);

	}

}
